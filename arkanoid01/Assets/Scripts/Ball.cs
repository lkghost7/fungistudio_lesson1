﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {
    [SerializeField] private Pad pad;


    private Vector3 ballOffset;

    private Rigidbody2D rb;
    private bool        enableBall;


    // Start is called before the first frame update
    void Start() {
        ballOffset = transform.position - pad.transform.position;
        rb         = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update() {
        if(!enableBall) {
            LockBall();
            
        }
        
        LanchBall();
    }

    private void LockBall() {
        transform.position = pad.transform.position + ballOffset;
    }

    public void LanchBall() {
        if(Input.GetMouseButtonDown(0)) {
            rb.velocity = new Vector2(1, 15);
            enableBall  = true;
        }
    }
}
