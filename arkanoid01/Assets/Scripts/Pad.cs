﻿using UnityEngine;

public class Pad : MonoBehaviour
{
    [SerializeField] private float _Min = -6f;
    [SerializeField] private float _Max = 6f;
        
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update() {
        Vector3 mousePosInPixels = Input.mousePosition;
        Vector3 mousePosUnits = Camera.main.ScreenToWorldPoint(mousePosInPixels);
        Vector3 newPadPos = new Vector3(mousePosUnits.x, transform.position.y);
        float newX = mousePosUnits.x;
        
        newX = Mathf.Clamp(newX, _Min, _Max );
        
        Vector3 newPadPos2 = new Vector3(newX, transform.position.y);

        // transform.position = newPadPos;
        transform.position = newPadPos2;
    }
}
