﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LoseGame : MonoBehaviour {
    // Start is called before the first frame update
    void Start() {
    }

    // Update is called once per frame
    void Update() {
        
    }

    private void OnCollisionEnter2D(Collision2D other) {
        Debug.Log("вошел в колизию");
        
        
    }

    private void OnCollisionExit2D(Collision2D other) {
        Debug.Log("вышел из колизии");
    }

    private void OnTriggerEnter2D(Collider2D other) {
        Debug.Log("вошел в триггер");
        SceneManager.LoadScene("WinScene");
    }

    private void OnTriggerExit2D(Collider2D other) {
        Debug.Log("вышел из тригера");
    }
}
