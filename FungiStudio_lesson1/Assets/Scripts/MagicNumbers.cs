﻿using UnityEngine;

public class MagicNumbers : MonoBehaviour{
    int         min = 1;
    int         max = 1000;
    int         guess;
    string      myName      = "Denis";
    bool        check       = false;
    private int _turnNumber = 0;

    // Start is called before the first frame update
    void Start() {
        if(max > (min + 1000)) {
            print("max > min");
        }
        else {
            print("max < min");
        }

        StartGame();
    }

    void StartGame() {
        min   = 100;
        max   = 1000;
        guess = CalculateGuess();
        Debug.Log("Hello, " + myName + ", welcome to Magic Numbers");
        print("Min: " + min          + ".");
        print("Max: "                + max);
        print("My guess is: "        + guess);
    }

    int CalculateGuess() {
        int newGuess = (min + max) / 2;
        return newGuess;
    }

    // Update is called once per frame
    void Update() {
        if(Input.GetKeyDown(KeyCode.UpArrow)) {
            min = guess;
            UpdateResult();
            _turnNumber += 1;
        }
        else if(Input.GetKeyDown(KeyCode.DownArrow)) {
            max = guess;
            UpdateResult();
            _turnNumber += 1;
        }
        else if(Input.GetKeyDown(KeyCode.Return)) {
            Debug.Log("I'm a genius!");
            StartGame();
            _turnNumber += 1;
        }

        if(guess == 500) {
            Debug.Log("Win");
        }
    }

    void UpdateResult() {
        guess = CalculateGuess();
        Debug.Log("Is it: " + guess + "?");
        Debug.Log("turn "           + _turnNumber.ToString());
    }
}
