﻿using TMPro;
using UnityEngine;

public class FunNumbers : MonoBehaviour {
    [SerializeField] private TextMeshPro m_3dText;
    [SerializeField] private TextMeshPro m_attempts;
    [SerializeField] private TextMeshPro m_One;
    [SerializeField] private TextMeshPro m_Tree;
    [SerializeField] private TextMeshPro m_Five;
    [SerializeField] private TextMeshPro m_Seven;
    [SerializeField] private TextMeshPro m_Nine;
    [SerializeField] private TextMeshPro m_EndGame;
    [SerializeField] private TextMeshPro m_StartGame;
    [SerializeField] private TextMeshPro m_WinGame;

    private int  _score = 0;
    private int  _attempts;
    private bool isStartGame;

    private void Awake() {
        m_EndGame.enabled = false;
        m_WinGame.enabled = false;
    }

    void Update() {
        ChekSpase();
        if(isStartGame) {
            return;
        }

        MoveInput();
        ScoreToString();
        EndGame();
        WinGame();
    }

    private void ChekSpase() {
        if(Input.GetKey(KeyCode.Space)) {
            isStartGame         = false;
            _score              = 0;
            _attempts           = 0;
            m_StartGame.enabled = false;
            m_EndGame.enabled   = false;
            m_WinGame.enabled   = false;

            m_One.color   = Color.white;
            m_Tree.color  = Color.white;
            m_Five.color  = Color.white;
            m_Seven.color = Color.white;
            m_Nine.color  = Color.white;
        }
    }

    public void ScoreToString() {
        m_3dText.text   = _score.ToString();
        m_attempts.text = _attempts.ToString();
    }

    public void EndGame() {
        if(_score > 50) {
            m_EndGame.renderer.enabled = true;
            m_EndGame.enabled          = true;
            _score                     = 999;
            isStartGame                = true;
        }
    }

    public void WinGame() {
        if(_score == 50) {
            m_WinGame.renderer.enabled = true;
            m_WinGame.enabled          = true;
            _score                     = 999;
            isStartGame                = true;
        }
    }

    public void MoveInput() {
        if(Input.GetKeyDown(KeyCode.Alpha1)) {
            _score              = _score    + 1;
            _attempts           = _attempts + 1;
            m_StartGame.enabled = false;
            m_One.color         = Color.red;
        }

        if(Input.GetKeyUp(KeyCode.Alpha1)) {
            m_One.color = Color.white;
        }

        if(Input.GetKeyDown(KeyCode.Alpha3)) {
            _score              = _score    + 3;
            _attempts           = _attempts + 1;
            m_StartGame.enabled = false;
            m_Tree.color        = Color.green;
        }

        if(Input.GetKeyUp(KeyCode.Alpha3)) {
            m_Tree.color = Color.white;
        }

        if(Input.GetKeyDown(KeyCode.Alpha5)) {
            _score              = _score    + 5;
            _attempts           = _attempts + 1;
            m_StartGame.enabled = false;
            m_Five.color        = Color.yellow;
        }

        if(Input.GetKeyUp(KeyCode.Alpha5)) {
            m_Five.color = Color.white;
        }

        if(Input.GetKeyDown(KeyCode.Alpha7)) {
            _score              = _score    + 7;
            _attempts           = _attempts + 1;
            m_StartGame.enabled = false;
            m_Seven.color       = Color.magenta;
        }

        if(Input.GetKeyUp(KeyCode.Alpha7)) {
            m_Seven.color = Color.white;
        }

        if(Input.GetKeyDown(KeyCode.Alpha9)) {
            _score              = _score    + 9;
            _attempts           = _attempts + 1;
            m_StartGame.enabled = false;
            m_Nine.color        = Color.cyan;
        }

        if(Input.GetKeyUp(KeyCode.Alpha9)) {
            m_Nine.color = Color.white;
        }
    }
}
