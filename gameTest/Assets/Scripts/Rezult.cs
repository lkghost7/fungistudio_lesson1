﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Rezult : MonoBehaviour {
    public int _answerTrue {get; set;}

    public int _answerFalse {get; set;}

    // Start is called before the first frame update
    void Start() {
        DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame
    void Update() {
        if(_answerFalse == 2) {
            SceneManager.LoadScene("GameOver");
            Destroy(gameObject);
        }
    }
}
