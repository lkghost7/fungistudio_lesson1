﻿using UnityEngine;
using UnityEngine.UI;

public class Answer : MonoBehaviour {
    [SerializeField] private Text   ScreenAnswerTrue;
    [SerializeField] private Text   ScreenAnswerFalse;
    private                  Rezult _rezult;


    void Start() {
        _rezult = FindObjectOfType<Rezult>();
    }

    // Update is called once per frame
    void Update() {
        GetScreenRezult();
    }

    public void GetScreenRezult() {
        ScreenAnswerTrue.text  = "Правильных  =  "   + _rezult._answerTrue.ToString();
        ScreenAnswerFalse.text = "Неправильных  =  " + _rezult._answerFalse.ToString();
    }
}
