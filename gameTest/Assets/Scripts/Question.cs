﻿using UnityEngine;
using UnityEngine.UI;

public class Question : MonoBehaviour {
    [SerializeField] private Text       AnswerText;
    [SerializeField] private Text       LifeText;
    [SerializeField] private GameObject DeleteButton1;
    [SerializeField] private GameObject DeleteButton2;


    private int    _anwerBool = 0;
    private Rezult _rezult;

    private
        // Start is called before the first frame update
        void Start() {
        _rezult = FindObjectOfType<Rezult>();
    }

    // Update is called once per frame
    void Update() {
        GetAnswer();
        GetLife();
    }

    public void GetAnswer() {
        if(_anwerBool == 1) {
            AnswerText.text = "Правильно !";
        }

        if(_anwerBool == 2) {
            AnswerText.text = "Неправильно !";
        }

        if(_anwerBool == 0) {
            AnswerText.text = "выберите ответ";
        }
    }

    public void SetAnswerTrue() {
        _anwerBool = 1;
        _rezult._answerTrue++;
    }

    public void SetAnswerFalse() {
        _anwerBool = 2;
        _rezult._answerFalse++;
    }

    public void GetLife() {
        LifeText.text = "Ошибки  =  " + _rezult._answerFalse.ToString();
    }

    public void DeleteAnswer() {
        DeleteButton1.SetActive(false);
        DeleteButton1.SetActive(false);
    }
}
