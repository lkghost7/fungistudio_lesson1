﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenLoad : MonoBehaviour {
    // Start is called before the first frame update
    void Start() {
    }

    // Update is called once per frame
    void Update() {
    }

    public void ScenLoader() {
        SceneManager.LoadScene("Game1");
    }

    public void ScenLoader2() {
        SceneManager.LoadScene("Game2");
    }

    public void ScenLoader3() {
        SceneManager.LoadScene("Game3");
    }

    public void ScenLoader4() {
        SceneManager.LoadScene("Result");
    }


    public void ExitGameTrue() {
        Application.Quit();
    }
}
