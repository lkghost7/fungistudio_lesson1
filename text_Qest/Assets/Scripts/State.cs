﻿using UnityEngine;


[CreateAssetMenu(menuName = "State")]
public class State : ScriptableObject 
{
    [TextArea(10, 25)][SerializeField] string storyText;
        [SerializeField] State[] nextStates;
        [SerializeField] State  location;
        

    public string GetStoryText()
    
    {
        return storyText;
    }

    public State[] GetNextStates()
    {
        return nextStates;
    }

    public State GetLocation() {
        return location;
    }
    
}