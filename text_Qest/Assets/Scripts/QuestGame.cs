﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class QuestGame : MonoBehaviour {
    [SerializeField] string gameTitle;
    // [SerializeField] Text   titleTextComponent2;
    [SerializeField] TextMeshProUGUI titleTextComponent;
    [SerializeField] Text   locateTextComponent;
    [SerializeField] Text   storyTextComponent;
    [SerializeField] State  startState;
    [SerializeField] State  startLocate;

    State currentState;

    // Start is called before the first frame update
    void Start() {
        currentState            = startState;
        titleTextComponent.text = gameTitle;
    }

    // Update is called once per frame
    void Update() {
        storyTextComponent.text = currentState.GetStoryText();
        locateTextComponent.text = currentState.GetLocation().GetStoryText();

        if(Input.GetKeyDown(KeyCode.Alpha1)) {
            currentState = currentState.GetNextStates()[0];
        }
        else if(Input.GetKeyDown(KeyCode.Alpha2)) {
            currentState = currentState.GetNextStates()[1];
        }
    }

    private void ManageStates() {
        for(int i = 0 ;
            i < currentState.GetNextStates().Length ;
            i++) {
            currentState = currentState.GetNextStates()[i];
            UpdateStateInfo();
        }
    }

    private void UpdateStateInfo() {
        storyTextComponent.text = currentState.GetStoryText();
    }
}
